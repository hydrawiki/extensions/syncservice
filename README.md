# SyncService
The SyncService Extension is a MediaWiki extension that acts as an independent job queue by extending the Job class.

## Installation
Download the latest release or master branch from this repository.  It should be placed in a folder called `SyncService` inside the `extensions/` folder in the MediaWiki installation.  Example: `extensions/SyncService/`

To load the extension place this line in the installation's LocalSettings.php file.
```php
wfLoadExtension('SyncService');
```

## Running the Worker
It is recommended to run the Worker with supervisor program such as supervisord, launchctl, god, or other similar programs.  While the Worker is very resilient any errors that bubble up from child processes may cause the Worker to crash or purposely exit.(Such as the Redis server going offline.)

Example: `php extensions/SyncService/classes/Worker.php`

The Worker class takes two optional arguments.
* --max-children=7
    * Maximum number of child processes to run at any given time.  Default of 7

* --queues=syncWorker:3,syncWorker:2,syncWorker:1,syncWorker:0
    * Comma separated list of queues to work on.  By defualt the Worker handles priority queuse of 3, 2, 1, and 0 with 3 being the highest priority.  Job classes can specify their priority or a custom queue.


## Using the Job class
Extending the Job class to create custom SyncService Jobs is easy.  It needs to extend SyncService\Job and provide one public method, execute() that takes an array of arguments.

Traditionally, back in 1.19, the MediaWiki job queue was not as flexible as it is in the latest versions.  SyncService was created to have job queues that run with having to do web calls to Special:RunJobs or be restricted to using the database for the queue.  As well this provides asynchronous job processing as multiple child processes can be ran at once.

```php
class ExampleJob extends SyncService\Job {
	/**
	 * Sets the default priority to normal. Overwrite in subclasses to run at a different priority.
	 * @var		int		sets the priority at which this service will run
	 */
	public static $priority = self::PRIORITY_NORMAL;

	/**
	 * Overwrite in subclasses and set to true to use a key in redis as a lock file.
	 * While the lock exists, other instances will immediately die with an error message.
	 * @var		bool	enables single-instance mode for a job
	 */
	public static $forceSingleInstance = false;

	/**
	 * Give this job a different name to use for its redis key when queueing. Overrides $priority setting.
	 * Useful when a job should be run by a custom worker instance, rather than the default SyncService worker.
	 * @var		string
	 */
	public static $customJobQueue;

	/**
	 * Example Job
	 *
	 * @access	public
	 * @param	array	Named arguments passed by the command that queued this job.
	 * - example_1	First argument passed to ExampleJob::queue().
	 * - example_2	Second argument passed to ExampleJob::queue().
	 * - ...
	 * @return	boolean	Success, reported to Worker class to set the exit status of the process.
	 */
	public function execute($args = []) {
		$example1	= $args['example_1'];
		$example2	= $args['example_2'];

		//Using a try catch is not required, but it can be useful for failure operations that may fail due to time outs.  In this case the job is automatically requeued to be tried again.
		try {
			//Do stuff.

			return ($success === false ? false : true); //Return a status so the Worker can see a process exit status.
		} catch (Exception $e) {
			self::queue($args); //Requeue in case of unintended failure.
			return false;
		}
	}
}
```

To queue a job for the ExampleJob class call the static queue() method off it with an array of arguments.  The return of the queue() function is if it was successfully queued.
```php
$success = ExampleJob::queue(
	[
		'example_1' => true,
		'example_2' => $data
	]
);
```