<?php
/**
 * Curse Inc.
 * Sync Service
 *
 * @author		Noah Manneschmidt
 * @copyright	(c) 2014 Curse Inc.
 * @license		GPL v3.0
 * @package		SyncService
 * @link		https://github.com/HydraWiki/SyncService
**/

namespace SyncService;

// simple class to replace the output functionality of the parent SyncWorker process
// when running a job syncronously
class OutputShim implements ILogger {
	/**
	 * Output Buffer
	 *
	 * @var		string
	 */
	public $output = '';

	/**
	 * Using output buffer.
	 *
	 * @var		boolean
	 */
	public $buffer = true;

	/**
	 * Output a line directly to the screen.
	 *
	 * @access	public
	 * @param	string	Message
	 * @param	integer	[Optional] Timestamp
	 * @return	void
	 */
	public function outputLine($message, $time = null) {
		if ($time) {
			$message = '['.date('c', $time).'] '.$message;
		}

		if ($this->buffer) {
			$this->output .= $message."\n";
		} else {
			echo $message."\n";
		}
	}
}
