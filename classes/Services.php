<?php
/**
 * Curse Inc.
 * Sync Service
 *
 * @author		Alexia E. Smith
 * @copyright	(c) 2018 Curse Inc.
 * @license		GPL v3.0
 * @package		SyncService
 * @link		https://github.com/HydraWiki/SyncService
**/

namespace SyncService;

class Services {
	/**
	 * Get configured services from the $wgSyncServices global.
	 * Does error checking for misconfigured services.
	 *
	 * @access	public
	 * @return	array	['ServiceShortName' => 'NamespacedClass']
	 */
	static public function getConfigured() {
		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		$services = (array) $config->get('SyncServices');
		asort($services);

		foreach ($services as $syncService) {
			if (!is_subclass_of($syncService, 'SyncService\Job', true)) {
				throw new \Exception("Error: {$syncService} is not a class that extends \SyncService\Job.");
			}
		}

		return $services;
	}
}