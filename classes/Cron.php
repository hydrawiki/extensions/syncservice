<?php
/**
 * Curse Inc.
 * Sync Service
 *
 * @author		Alexia E. Smith
 * @copyright	(c) 2017 Curse Inc.
 * @license		GPL v3.0
 * @package		SyncService
 * @link		https://github.com/HydraWiki/SyncService
**/

namespace SyncService;

class Cron {
	/**
	 * Schedule
	 *
	 * @var		array
	 */
	private $schedule = [
		'minutes'	=> '*',
		'hours'		=> '*',
		'days'		=> '*',
		'months'	=> '*',
		'weekdays'	=> '*',
		'arguments'	=> null
	];

	/**
	 * Main Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct($schedule = []) {
		if (!empty($schedule)) {
			foreach ($this->schedule as $period => $interval) {
				if (isset($schedule[$period])) {
					$function = 'set'.ucfirst(strtolower($period));
					$this->$function($schedule[$period]);
				}
			}
		}
	}

	/**
	 * Set minutes to run at.
	 *
	 * @access	public
	 * @param	mixed	Integer minutes or * for every minute.
	 * @return	void
	 */
	public function setMinutes($minutes = '*') {
		$this->validateInterval($minutes);
		$this->schedule['minutes'] = $minutes;
	}

	/**
	 * Set hours to run at.
	 *
	 * @access	public
	 * @param	mixed	Integer hours or * for every hour.
	 * @return	void
	 */
	public function setHours($hours = '*') {
		$this->validateInterval($hours);
		$this->schedule['hours'] = $hours;
	}

	/**
	 * Set days to run at.
	 *
	 * @access	public
	 * @param	mixed	Integer days or * for every day.
	 * @return	void
	 */
	public function setDays($days = '*') {
		$this->validateInterval($days);
		$this->schedule['days'] = $days;
	}

	/**
	 * Set months to run at.
	 *
	 * @access	public
	 * @param	mixed	Integer months or * for every month.
	 * @return	void
	 */
	public function setMonths($months = '*') {
		$this->validateInterval($months);
		$this->schedule['months'] = $months;
	}

	/**
	 * Set weekdays to run at.
	 *
	 * @access	public
	 * @param	mixed	Integer weekdays or * for every weekday.
	 * @return	void
	 */
	public function setWeekdays($weekdays = '*') {
		$this->validateInterval($weekdays);
		$this->schedule['weekdays'] = $weekdays;
	}

	/**
	 * Validate an interval.
	 *
	 * @access	private
	 * @param	mixed	Interval to validate.
	 * @return	boolean	True
	 */
	private function validateInterval($interval) {
		if ($interval !== '*' && intval($interval) < 0) {
			throw new MWException(__METHOD__.': '.$interval.' is an invalid interval.');
		}
		return true;
	}

	/**
	 * Set arguments.
	 *
	 * @access	public
	 * @param	array	Associative array of arugments to pass to the cron job.
	 * @return	void
	 */
	public function setArguments($arguments = null) {
		if (is_array($arguments)) {
			$this->schedule['arguments'] = $arguments;
		} elseif ($arguments !== null) {
			throw new MWException(__METHOD__.': Arguments should be passed as an associative array.');
		}
	}

	/**
	 * Get arguments for cron job.
	 *
	 * @access	public
	 * @return	array	Associative array of arugments to pass to the cron job.
	 */
	public function getArguments() {
		return $this->schedule['arguments'];
	}

	/**
	 * Should this cron run right now?
	 *
	 * @access	public
	 * @param	integer	[Optional] Override assumed time() timestamp.
	 * @return	boolean	If this cron should be ran.
	 */
	public function shouldRunNow($time = null) {
		if ($time === null) {
			$time = time();
		}

		if ($this->schedule['weekdays'] !== '*' && $this->schedule['weekdays'] != date('w', $time)) {
			return false;
		}

		if ($this->schedule['months'] !== '*' && $this->schedule['months'] != date('n', $time)) {
			return false;
		}

		if ($this->schedule['days'] !== '*' && $this->schedule['days'] != date('j', $time)) {
			return false;
		}

		if ($this->schedule['hours'] !== '*' && $this->schedule['hours'] != date('G', $time)) {
			return false;
		}

		if ($this->schedule['minutes'] !== '*' && $this->schedule['minutes'] != date('i', $time)) {
			return false;
		}
		return true;
	}
}
