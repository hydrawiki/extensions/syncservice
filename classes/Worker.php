<?php
/**
 * Curse Inc.
 * Sync Service
 *
 * @author		Noah Manneschmidt
 * @copyright	(c) 2014 Curse Inc.
 * @license		GPL v3.0
 * @package		SyncService
 * @link		https://github.com/HydraWiki/SyncService
**/

namespace SyncService;

use DynamicSettings\Environment;
use MediaWiki\MediaWikiServices;
use RedisCache;
use RedisConnectionPool;

require_once(dirname(__DIR__, 3).'/maintenance/Maintenance.php');
if (!interface_exists('\SyncService\ILogger')) {
	require_once(__DIR__.'/ILogger.php');
}


/**
 * Runs perpetually from the command line, waiting for requests to arrive in redis
 */
class Worker extends \Maintenance implements ILogger {
	/**
	 * Redis instance
	 *
	 * @var		object
	 */
	private $redis = false;

	/**
	 * Timestamp of last full log of ongoing process.
	 *
	 * @var		integer
	 */
	private $lastJobReport = 0;

	/**
	 * Report pending jobs every 5 minutes.
	 *
	 * @var		integer
	 */
	const JOB_REPORT_FREQ = 300;

	/**
	 * Default maximum number of child job processes.
	 *
	 * @var		string
	 */
	const DEF_MAX_CHILDREN = 7;

	/**
	 * Current runtime option for number of child job processes.
	 *
	 * @var		integer
	 */
	private $maxChildren;

	/**
	 * Array of running child workers PID => classname.
	 *
	 * @var		array
	 */
	private $children = [];

	/**
	 * Set to false when a halt signal is received, ending the main work loop.
	 *
	 * @var		boolean
	 */
	private $working = true;

	/**
	 * Set to true after forking into child processes.
	 *
	 * @var		boolean
	 */
	private $isChild = false;

	/**
	 * Schedule of services to run.
	 *
	 * @var		array
	 */
	private $schedules = [];

	/**
	 * Schedule lock outs after starting a scheduled job.
	 *
	 * @var		array
	 */
	private $scheduledLock = [];

	/**
	 * Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		parent::__construct();
		$this->addOption('max-children', 'Max number of child processes to spawn. Default: '.self::DEF_MAX_CHILDREN, false, true, 'c');
		$this->addOption('queues', 'Comma separated list of Redis job queues that this worker should take jobs from. Default: syncWorker:3,syncWorker:2,syncWorker:1,syncWorker:0', false, true, 'q');
		$this->children = [];
	}

	/**
	 * Main Execution
	 *
	 * @access	public
	 * @return	void
	 */
	public function execute() {
		$wgSyncServices = Services::getConfigured();

		//Bonus feature from PHP 5.5.
		if (function_exists('cli_set_process_title')) {
			@cli_set_process_title('SyncWorker Parent'); //Using error suppression is horrible, but unfortunately our testing environments run under OS X.  Without running as root OS X will not allow the process title to be changed.
		}

		// install signal handlers
		declare(ticks = 1); // we can instead use pcntl_signal_dispatch() in key locations for better performance?
		pcntl_signal(SIGABRT, [$this, 'haltSignalHandler']);
		pcntl_signal(SIGINT,  [$this, 'haltSignalHandler']);
		pcntl_signal(SIGQUIT, [$this, 'haltSignalHandler']);
		pcntl_signal(SIGTERM, [$this, 'haltSignalHandler']);
		//pcntl_signal(SIGPIPE, [$this, 'haltSignalHandler']); //@TODO: DynamicSettings ToolsSync trips this up for an unknown reason.
		pcntl_signal(SIGHUP,  [$this, 'haltSignalHandler']);
		// pcntl_signal(SIGCHLD, [$this, 'childSignalHandler']); // no real need to respond to this signal

		$jobsDomain = null; //Initialize

		// process cmd line args
		$childOpt = $this->getOption('max-children');
		if (intval($childOpt) > 0) {
			$this->maxChildren = $childOpt;
		} else {
			$this->maxChildren = self::DEF_MAX_CHILDREN;
		}

		// check registered sync services
		if (empty($wgSyncServices) || !is_array($wgSyncServices)) {
			$this->error("No sync services are registered. Add a class to \$wgSyncServices.\n");
			return;
		}

		foreach ($wgSyncServices as $syncService) {
			if ($syncService::getSchedule() !== false) {
				foreach ($syncService::getSchedule() as $index => $schedule) {
					$this->output("Adding scheduled job: #{$index} - {$syncService}\n");
					$this->schedules[$syncService][] = new Cron($schedule);
				}
			}
		}

		$this->redis = RedisCache::getClient('worker', [], true);
		if ($this->redis === false) {
			$this->outputLine('ERROR - Could not connect to Redis, failing.');
			exit(1);
		}

		//Close all existing database connections before going into our fork loop.
		$lbFactory = MediaWikiServices::getInstance()->getDBLoadBalancerFactory();
		$lbFactory->shutdown();

		$script = <<<LUA
local job
for i,k in ipairs(KEYS) do
	job = redis.call('rpop', k)
	if job then
		break
	end
end
return job
LUA;
		$redisSha = $this->redis->script('LOAD', $script);

		// our redis keys for each priority, in order
		$customQueues = $this->getOption('queues');
		$currentCheckout = Environment::detectCheckout();
		if (empty($customQueues)) {
			$queues = [
				'syncWorker:'.Job::PRIORITY_EMERG,
				'syncWorker:'.Job::PRIORITY_HIGH,
				'syncWorker', // old list key exists here at normal priority for compatibility until the list is empty
				'syncWorker:'.Job::PRIORITY_NORMAL,
				'syncWorker:'.Job::PRIORITY_LOW
			];
		} else {
			$queues = explode(',', $customQueues);
		}
		foreach ($queues as $index => $queue) {
			$queues[$index] = $currentCheckout.':'.$queue;
		}

		//Do the normal listening routine.
		$this->outputLine("SyncService Worker started up with max {$this->maxChildren} child threads. Watching Redis for jobs to be done.", time());

		while ($this->working) {
			try {
				$pong = $this->redis->ping();
			} catch (\RedisException $e) {
				$this->outputLine('WARNING - Redis connection is dead, attempting to reconnect.', time());
				$this->redis = RedisCache::getClient('worker', [], true);
			}

			if ($this->redis === false) {
				$this->outputLine('ERROR - Could not connect to Redis, failing.');
				exit(1);
			}

			try {
				//Attempt to pull a new job from Redis.
				$message = $this->redis->evalSha($redisSha, $queues, count($queues));
			} catch (\RedisException $e) {
				$this->outputLine('WARNING - Failed to evalSha() the worker queue.', time());
				$this->redis = RedisCache::getClient('worker', [], true);
			}

			$taskInfo = $this->decodeJobMessage($message);

			if ($taskInfo || $jobsDomain) {
				$this->forkForWork($taskInfo, $jobsDomain);
			} else {
				foreach ($this->schedules as $class => $schedules) {
					foreach ($schedules as $index => $cron) {
						$lockKey = 'syncService:cronLock:'.$index.':'.$class;
						$redisLock = boolval($this->redis->get($lockKey));
						if (isset($this->scheduledLock[$lockKey]) || $redisLock) {
							if (!isset($this->scheduledLock[$lockKey]) && $redisLock) {
								$this->scheduledLock[$lockKey] = $this->redis->ttl($lockKey);
							}
							$this->scheduledLock[$lockKey]--;
							if ($this->scheduledLock[$lockKey] < 0) {
								//$this->outputLine("Expired lock {$lockKey}", time());
								unset($this->scheduledLock[$lockKey]);
							} else {
								//$this->outputLine("{$this->scheduledLock[$lockKey]} seconds remaining for {$lockKey}", time());
								continue;
							}
							if ($redisLock) {
								//Technically this code is unreachable 99% of the time.
								$this->outputLine("An instance of {$lockKey} is already running on another server.  Skipping running scheduled task.", time());
								continue;
							}
						}
						if ($cron->shouldRunNow()) {
							$arguments = $cron->getArguments();
							$taskInfo = [
								'class'	=> $class,
								'args'	=> (is_array($arguments) ? $arguments : [])
							];
							$this->scheduledLock[$lockKey] = 60;
							$this->forkForWork($taskInfo, $jobsDomain);
							$this->redis->setEx($lockKey, 60, 1);
							$this->outputLine('Running scheduled job: '.$class, time());
						}
					}
				}
				$this->outputLine('No work is currently available, sleeping for one second.', time());
				sleep(1);
			}

			// Clean up any jobs that have finished
			while ($this->retireChild());

			// stop creating more workers if we are at maximum
			if (count($this->children) > $this->maxChildren-1) {
				$this->outputJobList();
				$this->outputLine('Max children reached. Waiting for a child to finish...', time());
				$this->retireChild(true);
			}
		}

		$this->outputLine('Love is over. Waiting on children to finish...', time());

		while (count($this->children)) {
			$this->outputJobList();
			$this->retireChild(true);
		}
	}

	/**
	 * Fork a new worker.
	 *
	 * @access	public
	 * @param	array	Task Information
	 * @param	mixed	Wiki Domain
	 * @return	void
	 */
	private function forkForWork($taskInfo, $jobsDomain) {
		$newPid = pcntl_fork();

		if ($newPid === 0) {
			//Child
			$this->isChild = true;

			//We actually have a SyncService job from Redis.
			if (is_array($taskInfo)) {
				//Overwrite old dead connections with the new ones.
				RedisConnectionPool::destroySingletons();
				$this->redis = RedisCache::getClient('worker', [], true);

				//Do the work!
				$status = $this->runTask($taskInfo);

				//Close extra connections before dying.
				$lbFactory = MediaWikiServices::getInstance()->getDBLoadBalancerFactory();
				$lbFactory->shutdown();
			} else {
				//I have no idea what $jobsDomain does or was used for.  It does not appear to be used anymore.  --Alexia 2019-01-15
				$status = $jobsDomain;
			}

			//Do not let this child survive.
			exit(intval($status));

		} elseif ($newPid === -1 || $newPid === null) {
			//Something failed.
			$this->outputLine('Forking failed!  Running serialized...', time());
			$status = $this->runTask($taskInfo);
			$this->outputLine($status ? 'Success!' : 'Failure!', time());
		} else {
			//Parent saves info about child processes
			if (is_array($taskInfo)) {
				$this->outputLine("Spawned child process {$newPid} for {$taskInfo['class']} with params: ".var_export($taskInfo['args'], true), time());
				$this->children[$newPid] = $taskInfo['class'];
			} else {
				$this->outputLine("Spawned child process {$newPid} for job runner on {$jobsDomain}.", time());
				$this->children[$newPid] = 'Job Runner for '.$jobsDomain;
			}
		}
	}

	public function haltSignalHandler($sigCode) {
		if ($this->isChild) {
			return;
		}
		$this->working = false;
		$this->outputLine('Caught halt signal: '.$sigCode, time());
		//Children should be cleaned up automatically.
	}

	/**
	 * Cleans up a finished child worker process and removes it from the child list.
	 *
	 * @access	private
	 * @param	boolean	[Optional], if true this will block until a child finishes
	 * @return	boolean	True if child collected, false if not
	 */
	private function retireChild($wait = false) {
		$flags = 0;
		$status = null;
		if (!$wait) {
			$flags = WNOHANG;
		}

		$pid = pcntl_wait($status, $flags);

		if ($pid > 0) {
			$this->cleanupAfterChild($pid, $status);
			return true;
		}
		return false;
	}

	/**
	 * Removes child from our list of children
	 *
	 * @access	private
	 * @param	integer	process id
	 * @param	integer	optional status id returned from pcntl_wait
	 * @param	integer	optional status code returned by child process
	 */
	private function cleanupAfterChild($pid = 0, $statusId = null, $statusCode = null) {
		if ($statusId !== null) {
			$statusCode = pcntl_wexitstatus($statusId);
		}
		$this->outputLine($this->children[$pid].' (process '. $pid.') exited with '.$statusCode.'.', time());
		unset($this->children[$pid]);
	}

	private function outputJobList() {
		// Job list reporting is throttled
		if (time()-$this->lastJobReport > self::JOB_REPORT_FREQ) {
			$this->lastJobReport = time();
			$this->outputLine('Full report on pending jobs:', time());
			foreach ($this->children as $savedPid => $job) {
				$this->outputLine($job.' is running as process '.$savedPid, time());
			}
		}
	}

	/**
	 * Attempt to decode a job message retrieved from redis
	 * @param array $message raw response from redis with serialized array expected in $message[1]
	 * @return array|null
	 */
	private function decodeJobMessage($message) {
		if (!is_null($message) && !empty($message) && is_string($message)) {
			$data = unserialize($message);
			if (is_array($data)) {
				return $data;
			}
		}
		return null;
	}

	/**
	 * Run a queued task
	 * @param array $taskInfo encoded and pushed into redis by Job::queue
	 * @return int  return code provided by the result of the job (non-zero indicates failure)
	 */
	private function runTask($taskInfo) {
		global $wgSyncServices;

		if (in_array($taskInfo['class'], $wgSyncServices)) {
			if ($this->isChild && function_exists('cli_set_process_title')) {
				@cli_set_process_title('SyncWorker Child ('.$taskInfo['class'].')');
			}

			if (!Job::_beginInstance($taskInfo['class'], $this)) {
				return 1;
			}

			$service = new $taskInfo['class']($this);
			$service->executionStyle = 'queue';

			//Actually run the task.
			$result = $service->execute($taskInfo['args']);

			Job::_endInstance($taskInfo['class'], $this);
			return $result;
		} else {
			$this->error('Ignoring job for unknown service: '.$taskInfo['class']);
			return 1;
		}
	}

	public function outputLine($message, $time = null) {
		if ($this->isChild) {
			$message = '(child'.posix_getpid().') '.$message;
		}
		if ($time) {
			$message = '['.date('c', $time).'] '.$message;
		}
		parent::output($message."\n");
	}
}

$maintClass = 'SyncService\Worker';
require_once( RUN_MAINTENANCE_IF_MAIN );