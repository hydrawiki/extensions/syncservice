<?php
/**
 * Curse Inc.
 * Sync Service
 *
 * @author		Noah Manneschmidt
 * @copyright	(c) 2014 Curse Inc.
 * @license		GPL v3.0
 * @package		SyncService
 * @link		https://github.com/HydraWiki/SyncService
**/

namespace SyncService;

interface ILogger {
	/**
	 * Send a message to a relevant output location
	 *
	 * @param	string	the message to output
	 * @param	int		[optional] timestamp with which to prefix the message
	 * @return	void
	 */
	public function outputLine($message, $time = null);
}
