<?php
/**
 * Curse Inc.
 * Sync Service
 *
 * @author		Noah Manneschmidt
 * @copyright	(c) 2014 Curse Inc.
 * @license		GPL v3.0
 * @package		SyncService
 * @link		https://github.com/HydraWiki/SyncService
**/

namespace SyncService;

use DynamicSettings\Environment;
use RedisCache;

/**
 * A new instance will be created for each service that needs to run
 * Use the queue
 */
abstract class Job {
	/**
	 * Priority constants. Override the $priority property with one of these in subclasses.
	 *
	 * @var		constant
	 */
	const PRIORITY_LOW    = 0;
	const PRIORITY_NORMAL = 1;
	const PRIORITY_HIGH   = 2;
	const PRIORITY_EMERG  = 3;

	/**
	 * Sets the default priority to normal. Overwrite in subclasses to run at a different priority.
	 *
	 * @var		int		sets the priority at which this service will run
	 */
	public static $priority = self::PRIORITY_NORMAL;

	/**
	 * Overwrite in subclasses and set to true to use a key in redis as a lock file.
	 * While the lock exists, other instances will immediately die with an error message.
	 *
	 * @var		bool	enables single-instance mode for a job
	 */
	static public $forceSingleInstance = false;

	/**
	 * Give this job a different name to use for its redis key when queueing. Overrides $priority setting.
	 * Useful when a job should be run by a custom worker instance, rather than the default SyncService worker.
	 *
	 * @var		string
	 */
	static public $customJobQueue;

	/**
	 * Redis instances created to be used in a separate thread.  The same objects available as  $this->redis.
	 * @TODO: Maybe soon to be deprecated?  Per Noah's original note.
	 *
	 * @var		string
	 */
	protected $redis = false;

	/**
	 * Reference to the ILogger instance that will be handling logging for this job.
	 *
	 * @var		object
	 */
	protected $logger;

	/**
	 * String giving the name of the static function invoking the job ('run' or 'queue').
	 *
	 * @var		string
	 */
	public $executionStyle;

	/**
	 * Periodic schedule to run like a cron job.  Leave as false to not have a schedule.
	 * Multiple schedule can be defined.
	 * Optional arguments to the job can be passed as well.
	 * [
	 *		[
	 *			'minutes' => '*',
	 *			'hours' => '*',
	 *			'days' => '*',
	 *			'months' => '*',
	 *			'weekdays' => '*',
	 *			'arguments' => []
	 *		]
	 * ]
	 *
	 * @var		array
	 */
	static public $schedule = false;

	/**
	 * Returns the string key to use for a given job class.
	 * Will be namespaced to the current environment.
	 *
	 * @access	public
	 * @param	string	Class Name
	 * @return	string
	 */
	final static public function _lockKeyForClass($class) {
		$currentCheckout = Environment::detectCheckout();
		return $currentCheckout.':syncService:lockInstance:'.$class;
	}

	/**
	 * Performs logic for enforcing single-instance mode
	 *
	 * @param	string	Name of the job class
	 * @param	ILogger	Logger object
	 * @return	bool	True if execution should continue, false if execution should halt
	 */
	static public function _beginInstance($class, ILogger $output) {
		if ($class::$forceSingleInstance) {
			$redis = RedisCache::getClient('worker');
			if ($redis->exists(self::_lockKeyForClass($class))) {
				$output->outputLine("An instance of $class is already running. This one will exit.", time());
				return false;
			} else {
				// set the lock key
				$redis->set(self::_lockKeyForClass($class), 1);
				// expire the key in 20 hours just in case it crashes and we don't get to del the key
				$redis->expire(self::_lockKeyForClass($class), 72000);
			}
		}
		return true;
	}

	/**
	 * Cleans up single-instance lock if needed after a job completes
	 *
	 * @param	ILogger	Logger object
	 * @param	string	Name of the job class
	 */
	static public function _endInstance($class, ILogger $output) {
		if ($class::$forceSingleInstance) {
			$redis = RedisCache::getClient('worker');
			$redis->del(self::_lockKeyForClass($class));
		}
	}

	/**
	 * Create a new instance of a job
	 *
	 * @access	public
	 * @param	object	ILogger instance (see be OutputShim defined above or SyncWorker class)
	 * @return	void
	 */
	public function __construct(ILogger $logger) {
		$this->logger = $logger;
	}

	/**
	 * Pushes a job into redis to be performed by the worker
	 *
	 * @param	array	An array that will be serialized, pushed into redis, and passed to the execute method.
	 * @param	string	Checkout override
	 * @return	boolean success value (probably just whether redis connection was available)
	 */
	public static function queue($args = [], $customCheckout = null) {
		$class = get_called_class();
		if ($class === 'SyncService\Job') {
			throw new \Exception('Queue may only be called on subclasses of SyncService\Job.');
		}

		$redis = RedisCache::getClient('worker');

		// Check for custom redis queue key
		if (is_string($class::$customJobQueue)) {
			$redisKey = $class::$customJobQueue;
		} else {
			$redisKey = 'syncWorker:'.$class::$priority;
		}
		$currentCheckout = $customCheckout !== null ? $customCheckout : Environment::detectCheckout();
		$redisKey = $currentCheckout.':'.$redisKey;

		return $redis !== false && is_numeric($redis->lPush($redisKey, serialize(['class' => $class, 'args' => $args])));
	}

	/**
	 * Runs a job syncronously, in-process
	 *
	 * @param	array	Params that will be passed to the job
	 * @param	bool	return output in a string instead of writing directly to STDOUT
	 * @param	int		will contain the integer return value of the job after completion
	 * @return	string	The output that would have been written to STDOUT
	 */
	static public function run($args = [], $bufferOutput = true, &$returnValue = null) {
		$class = get_called_class();
		if ($class === 'SyncService\Job') {
			throw new \Exception('Run may only be called on subclasses of SyncService\Job.');
		}

		$redis = RedisCache::getClient('worker');
		$outputShim = new OutputShim();
		$outputShim->buffer = $bufferOutput;

		if (!self::_beginInstance($class, $outputShim)) {
			return $outputShim->output;
		}

		$job = new $class($outputShim);
		$job->redis = $redis;
		$job->executionStyle = 'run';

		$returnValue = $job->execute($args);

		self::_endInstance($class, $outputShim);

		return $outputShim->output;
	}

	/**
	 * Sends output to STDOUT prefixed by the subclass name, optionally timestamped
	 *
	 * @param	string	the message to output
	 * @param	int		optional unix timestamp to output
	 * @return	void
	 */
	protected function outputLine($message, $time = null) {
		$this->logger->outputLine('['.get_class($this).'] '.$message, $time);
	}

	/**
	 * Return cron schedule if applicable.
	 *
	 * @access	public
	 * @return	mixed	False for no schedule or an array of schedule information.
	 */
	static public function getSchedule() {
		return static::$schedule;
	}

	/**
	 * Implement this and access Redis via $this->redis.
	 *
	 * @param	array	the same array that was passed to self::queue()
	 * @return	integer	the exit value that this process fork should use
	 */
	abstract public function execute($args);
}
