<?php
/**
 * Curse Inc.
 * SyncService
 * Invoke any SyncService service/job to run immediately inline.
 *
 * @author		Alexia E. Smith
 * @copyright	(c) 2018 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		All Sites
 * @link		https://gitlab.com/hydrawiki
 *
 **/

namespace SyncService;

require_once(dirname(__DIR__, 3)."/maintenance/Maintenance.php");

class RunJob extends \Maintenance {
	/**
	 * Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		parent::__construct();

		$this->mDescription = "Run a defined SyncService job.";
		$this->addArg('job', "Job name as defined in the extension's extension.json.", false);
	}

	/**
	 * Trigger all wikis to be counted for users.
	 *
	 * @access	public
	 * @return	integer	Exit Status Code.
	 */
	public function execute() {
		$services = Services::getConfigured();
		$job = $this->getArg(0);
		if (empty($job) || !in_array($job, $services)) {
			$this->output("Please provide a valid name of a job to run from the list of available jobs:\n\n");
			foreach ($services as $job) {
				$this->output($job."\n");
			}
			return 0;
		}
		// get arguments to pass to job
		$arg = $this->getArg(1);
		$decoded = json_decode($arg, true);
		if ($arg && !$decoded) {
			$this->output("Please provide a valid json string to pass arguments to the job.\n");
			return 1;
		}
		$args = $arg && $decoded ? $decoded : [];
		
		$exitCode = 0;
		$job::run($args, false, $exitCode);
		return $exitCode;
	}
}

$maintClass = 'SyncService\RunJob';
require_once(RUN_MAINTENANCE_IF_MAIN);
